# README #

# App Development: Android #
## Fall 2015: Activity 2 ##

### Description ###

The objective of this assignment is to build a calculator app.

The app has two activites:

1. Activity to show the number display and buttons.
2. Activity to set some app preferences.

Functionality:

1. The calculator should be able to add and subtract.
2. The number buttons (0-9) are used for entering the digits.
3. The '+' and the '-' are used to add and subtract, respectively.
4. The 'B' button erases the last character entered from the display.
5. The 'C' button clears the display.
6. The '=' buttons does the computation and displays it.

Notes:

1. The app should disallow two consecutive + and - signs to be entered.
2. The app should be able to run on all Android API from version 19 and up.
