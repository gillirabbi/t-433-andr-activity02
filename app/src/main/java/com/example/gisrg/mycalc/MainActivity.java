package com.example.gisrg.mycalc;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.math.BigInteger;
import java.util.StringTokenizer;

public class MainActivity extends AppCompatActivity {

    private TextView mDisplay;
    private Vibrator mVibrator;
    private Boolean mUseVibrator = false;
    private Boolean negativeValue = false;
    SharedPreferences mSP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDisplay = (TextView) findViewById(R.id.display);
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/digital-7.ttf");
        mDisplay.setTypeface(typeFace);
        mVibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
        mSP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
    }

    @Override
    protected void onStart() {
        super.onStart();
        mUseVibrator = mSP.getBoolean("vibrate", false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, MyCalcPreferenceActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void buttonPressed(View view) {
        Button buttonView = (Button) view;
        String text;

        if (negativeValue) {
            String tmp = mDisplay.getText().toString();
            mDisplay.setText("0" + tmp);
            negativeValue = false;
        }

        switch (view.getId()) {
            case R.id.button0:
                mDisplay.append(buttonView.getText());
                break;
            case R.id.button1:
                mDisplay.append(buttonView.getText());
                break;
            case R.id.button2:
                mDisplay.append(buttonView.getText());
                break;
            case R.id.button3:
                mDisplay.append(buttonView.getText());
                break;
            case R.id.button4:
                mDisplay.append(buttonView.getText());
                break;
            case R.id.button5:
                mDisplay.append(buttonView.getText());
                break;
            case R.id.button6:
                mDisplay.append(buttonView.getText());
                break;
            case R.id.button7:
                mDisplay.append(buttonView.getText());
                break;
            case R.id.button8:
                mDisplay.append(buttonView.getText());
                break;
            case R.id.button9:
                mDisplay.append(buttonView.getText());
                break;
            case R.id.buttonPlus:
                text = mDisplay.getText().toString();
                if (text.isEmpty()) {
                    break;
                }
                //Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
                if (!text.endsWith("+") && !text.endsWith("-")) {
                    mDisplay.append(buttonView.getText());
                }
                break;
            case R.id.buttonMinus:
                text = mDisplay.getText().toString();
                if (text.isEmpty()) {
                    mDisplay.append("0" + text);
                }
                //Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
                if (!text.endsWith("+") && !text.endsWith("-")) {
                    mDisplay.append(buttonView.getText());
                }
                break;
            case R.id.buttonBack:
                text = mDisplay.getText().toString();
                if (!text.isEmpty()) {
                    text = text.substring(0, text.length()-1);
                    mDisplay.setText(text);
                }
                break;
            case R.id.buttonClear:
                mDisplay.setText("");
                break;
            case R.id.buttonEqual:
                text = mDisplay.getText().toString();
                if (text.endsWith("+") || text.endsWith("-")) {
                    text = text.substring(0, text.length()-1);
                    mDisplay.setText(text);
                }
                String result = evaluateExpression(mDisplay.getText().toString());
                mDisplay.setText(result);

                if (Integer.parseInt(result) < 0) {
                    negativeValue = true;
                }
                break;
        }

        if (mUseVibrator) {
            mVibrator.vibrate(200);
            //Toast.makeText(getApplicationContext(), "Vibrate...", Toast.LENGTH_LONG).show();
        }
    }

    String evaluateExpression(String expr) {
        //BigInteger result = BigInteger.ZERO;
        StringTokenizer st = new StringTokenizer(expr, "[+\\-]", true);
        String operator;
        Integer result = Integer.parseInt(st.nextToken());

        while (st.hasMoreElements()) {
            operator = st.nextToken();
            if (operator.equals("+")) {
                result += Integer.parseInt(st.nextToken());
            } else if (operator.equals("-")) {
                result -= Integer.parseInt(st.nextToken());
            }
        }
        return result.toString();
    }
}
