package com.example.gisrg.mycalc;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Created by gisrg on 8/28/2015.
 */
public class MyCalcPreferenceActivity extends PreferenceActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}
